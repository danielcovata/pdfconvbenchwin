﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace piaconv
{
    class Program
    {
        public static void HideWindow(string title)
        {
            // Get a handle to the Calculator application.
            IntPtr handle = Native.FindWindow(null, title);

            // Verify that Calculator is a running process.
            if (handle == IntPtr.Zero)
            {
                return;
            }
            Native.ShowWindow(handle, Native.ShowWindowCommands.Hide);
        }


        static void ExportWord2Pdf(Word.Document document, string outPath)
        {
#if false
            document.ExportAsFixedFormat(outPath, WdExportFormat.wdExportFormatPDF);
#else

#endif
        }
        static void ConvertWord(string file, string outFile)
        {
            Word.Application wordApp = null;
            Word.Documents documents = null;
            Word.Document document = null;
            object missing = Type.Missing;
            try
            {
                wordApp = new Word.Application();
                documents = wordApp.Documents;
                document = documents.Open(
                    FileName: file,
                    ConfirmConversions: false,
                    ReadOnly: true,
                    AddToRecentFiles: false,
                    PasswordDocument: "bad",
                    PasswordTemplate: "bad",
                    Revert: true,
                    WritePasswordDocument: "bad",
                    WritePasswordTemplate: "bad",
                    Format: Word.WdOpenFormat.wdOpenFormatAuto,
                    Encoding: missing,
                    Visible: false,
                    OpenAndRepair: true,
                    DocumentDirection: missing,
                    NoEncodingDialog: true,
                    XMLTransform: missing);

                if (document != null) {
#if DEBUG
                    Console.WriteLine("Opened document {0}", file);
#endif

                    document.ExportAsFixedFormat(
                        OutputFileName: outFile,
                        ExportFormat: Word.WdExportFormat.wdExportFormatPDF,
                        OpenAfterExport: false,
                        OptimizeFor: Word.WdExportOptimizeFor.wdExportOptimizeForOnScreen,
                        Range: Word.WdExportRange.wdExportAllDocument,
                        From: 1,
                        To: 1,
                        Item: Word.WdExportItem.wdExportDocumentContent,
                        IncludeDocProps: false,
                        KeepIRM: true,
                        CreateBookmarks: Word.WdExportCreateBookmarks.wdExportCreateWordBookmarks,
                        DocStructureTags: true,
                        BitmapMissingFonts: true,
                        UseISO19005_1: false,
                        FixedFormatExtClassPtr: missing);
                }
#if DEBUG
                Console.WriteLine("Exported {0}", outFile);
#endif
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("error converting {0}: {1}", file, e.ToString()));
            }
            finally
            {
                if (document != null) {
                    ((Word._Document) document).Close(
                        Word.WdSaveOptions.wdDoNotSaveChanges,
                        Word.WdOriginalFormat.wdOriginalDocumentFormat,
                        false);
#if DEBUG
                    Console.WriteLine("Closed document {0}", file);
#endif
                    Marshal.FinalReleaseComObject(document);
                }
                if (documents != null) {
                    Marshal.FinalReleaseComObject(documents);
                }
                if (wordApp != null)
                {
                    ((Word._Application)wordApp).Quit();
                    Marshal.FinalReleaseComObject(wordApp);
                }
            }
        }

        private static void ConvertExcel(string file, string outFile)
        {
            Excel.Application app = null;
            Excel.Workbook workbook = null;
            Excel.Workbooks workbooks = null;
            object missing = Type.Missing;
            try
            {
                app = new Excel.Application();
                app.DisplayAlerts = false;
                //app.DisplayClipboardWindow = false;
                //app.DisplayCommentIndicator = 

                // Don't use this is, only works for Smart workbooks
                //app.DisplayDocumentActionTaskPane = false;

                workbooks = app.Workbooks;

                // see http://msdn.microsoft.com/en-us/library/office/microsoft.office.interop.excel.workbooks.open%28v=office.14%29.aspx
                workbook = workbooks.Open(
                    Filename: file,
                    // create charts from attached graphs
                    UpdateLinks: 2,

                    ReadOnly: true,
                    Format: missing,
                    // omitting value raises UI to 'enter password'.  Does this raise exception instead?
                    Password: "bad",
                    WriteResPassword: "bad",
                    IgnoreReadOnlyRecommended: true,
                    Origin: missing,
                    Delimiter: missing,
                    Editable: false,
                    Notify: false,
                    Converter: missing,
                    AddToMru: false,
                    Local: false,
                    CorruptLoad: missing);

                if (workbook != null)
                {
#if DEBUG
                    Console.WriteLine("Opened document {0}", file);
#endif
                    /*
                     * crude method to hide the publishing window. we don't want
                     * to use this because the window will always momentarily appear.
                     * a better approach, using WinEventHook is demonstrated in
                     * an adjacent program - hidepublishing, which receives
                     * an event for the window opening and then hides it.
                     * * 
                    Task.Run(
                        async () => {
                            for (int i = 0; i < 10; ++i)
                            {
                                await Task.Delay(50);
                                HideWindow("Publishing...");
                            }
                        });
                     */
                    workbook.ExportAsFixedFormat(
                        Excel.XlFixedFormatType.xlTypePDF,
                        Filename: outFile,
                        Quality: Excel.XlFixedFormatQuality.xlQualityStandard,
                        IncludeDocProperties: true,
                        IgnorePrintAreas: true,
                        From: missing,
                        To: missing,
                        OpenAfterPublish: false,
                        FixedFormatExtClassPtr: missing);
                    #if DEBUG
                    Console.WriteLine("Exported {0}", outFile);
#endif
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("error converting {0}: {1}", file, e.ToString()));
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close(SaveChanges: false, Filename: missing, RouteWorkbook: false);
                    Marshal.FinalReleaseComObject(workbook);
                    workbook = null;
                }

                if (workbooks != null)
                {
                    Marshal.FinalReleaseComObject(workbooks);
                    workbooks = null;
                }

                if (app != null)
                {
                    ((Excel._Application)app).Quit();
                    Marshal.FinalReleaseComObject(app);
                    app = null;
                }
            }
#if DEBUG
            Console.WriteLine("Closed document {0}", file);
#endif
        }

        private static void ConvertSlide(string path, string outFile)
        {

            // See http://stackoverflow.com/questions/158706/how-to-properly-clean-up-excel-interop-objects
            // for why we do this.
            PowerPoint.Application app = null;
            PowerPoint.Presentations presentations = null;
            PowerPoint.Presentation document = null;

            object missing = Type.Missing;

            try
            {
                app = new PowerPoint.Application();
                app.DisplayAlerts = PowerPoint.PpAlertLevel.ppAlertsNone;

                presentations = app.Presentations;

                document = presentations.Open(
                    FileName: path,
                    ReadOnly: MsoTriState.msoTrue,
                    Untitled: MsoTriState.msoTrue,
                    WithWindow: MsoTriState.msoFalse);
                if (document != null) {
                    var started = DateTime.Now;
                    document.ExportAsFixedFormat(
                        Path: outFile,
                        FixedFormatType: PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF,
                        Intent: PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentScreen,
                        FrameSlides: MsoTriState.msoFalse,

                        HandoutOrder: PowerPoint.PpPrintHandoutOrder.ppPrintHandoutHorizontalFirst,
                        OutputType: PowerPoint.PpPrintOutputType.ppPrintOutputSlides,
                        PrintHiddenSlides: MsoTriState.msoFalse,
                        PrintRange: null,
                        RangeType: PowerPoint.PpPrintRangeType.ppPrintAll,
                        SlideShowName: "",
                        IncludeDocProperties: false,
                        KeepIRMSettings: true,
                        DocStructureTags: true,
                        BitmapMissingFonts: true,
                        UseISO19005_1: false,
                        ExternalExporter: missing);
                    var finished = DateTime.Now;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("error converting {0}: {1}", path, e.ToString());
            }
            finally
            {
                if (document != null)
                {
                    document.Close();
                    Marshal.FinalReleaseComObject(document);
                }
                if (presentations != null)
                {
                    Marshal.FinalReleaseComObject(presentations);
                }
                if (app != null)
                {
                    ((PowerPoint._Application)app).Quit();
                    Marshal.FinalReleaseComObject(app);
                }
            }
        }

        static void ConvertDocument(string path, string outFolder)
        {
            string ext = Path.GetExtension(path);
            var name = Path.GetFileNameWithoutExtension(path);
            var outFile = Path.Combine(outFolder, name + ".pdf");
            var started = DateTime.Now;

            if (ext == ".doc" || ext == ".docx")
            {
                ConvertWord(path, outFile);
            }
            else if (ext == ".xls" || ext == ".xlsx" || ext == ".csv")
            {
                ConvertExcel(path, outFile);
            }
            else if (ext == ".ppt" || ext == ".pptx")
            {
                ConvertSlide(path, outFile);
            }
            else
            {
#if DEBUG
                Console.WriteLine("ignoring " + path);
#endif
            }
            var finished = DateTime.Now;
            Console.WriteLine("conversion_time, {0}, {1}", Path.GetFileName(path), (finished - started).TotalMilliseconds);
        }

        static void Main(string[] args)
        {

            if (args.Length < 2)
            {
                Console.WriteLine("Usage: piaconv <infolder> <outfolder> [#threads]");
                return;
            }
            var inFolder = args[0];
            var outFolder = args[1];
            var parallelism = args.Length < 3 ? 1 : int.Parse(args[2]);

            var started = DateTime.Now;

            var files = Directory.GetFiles(inFolder).Where(s => !Path.GetFileName(s).StartsWith("~$"));
            var options = new ParallelOptions() { MaxDegreeOfParallelism = parallelism };

            if (parallelism == 1)
            {
                foreach (var file in files)
                {
                    ConvertDocument(file, outFolder);
                }

            }
            else
            {
                /*
                //TODO - demo purposes only, in practice we'd just create a pool of word processes
                // and queue them
                Parallel.ForEach(
                    files,
                    options,
                    file => {
                        var wordApp = new Word.Application();
                        ConvertWord(wordApp, file, outFolder);
                        ((Word._Application)wordApp).Quit();
                    });
                 * */
            }
            var span = DateTime.Now - started;
            Console.WriteLine("total_conversion_time {0}", span);
        }
    }
}
