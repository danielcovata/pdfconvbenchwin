﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winevents
{
    static class Program
    {
        public static void HideWindowByName(string title)
        {
            IntPtr handle = Native.FindWindow(null, title);
            if (handle == IntPtr.Zero) {
                return;
            }
            Native.ShowWindow(handle, Native.ShowWindowCommands.Hide);
        }

        static void WinEventProc(IntPtr hWinEventHook, uint eventType,
            IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime) {
            // filter out non-HWND namechanges... (eg. items within a listbox)
            if (idObject != 0 || idChild != 0) {
                return;
            }
            var text = GetText(hwnd) ?? "unknown";
            Console.WriteLine("Foreground: {0}", text);

            if (text == "Publishing...") {
                Native.ShowWindow(hwnd, Native.ShowWindowCommands.Hide);
            }
        }

        public static string GetText(IntPtr hWnd)
        {
            // Allocate correct string length first
            var length = Native.GetWindowTextLength(hWnd);
            var sb = new StringBuilder(length + 1);
            Native.GetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        // Need to ensure delegate is not collected while we're using it,
        // storing it in a class field is simplest way to do this.
        static Native.WinEventDelegate procDelegate = WinEventProc;


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            
            // Listen for name change changes across all processes/threads on current desktop...
            // Need a message pump for the hook to work.
            IntPtr hhook = Native.SetWinEventHook(Native.EVENT_SYSTEM_FOREGROUND,
                Native.EVENT_SYSTEM_FOREGROUND,
                IntPtr.Zero,
                procDelegate, 0, 0, Native.WINEVENT_OUTOFCONTEXT);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            
            Native.UnhookWinEvent(hhook);
        }
    }
}
