﻿module faspose =    
    open Aspose.Words    
    open Aspose.Cells
    open Aspose.Slides
    open System.IO
    open FSharp.Collections.ParallelSeq    

    type Args = { inputFolder: string; outputFolder: string }

    let parseArgs = function
        | inStr::outStr::[] -> 
            Some { inputFolder = inStr; outputFolder = outStr }
        | _ -> None

    let convertWord (src:string, dst:string) :unit =        
        Document(src).Save(dst) |> ignore

    let convertSpreadsheet (src:string, dst:string) :unit = 
        Aspose.Cells.Workbook(src).Save(dst) |> ignore        

    let convertSlide  (src:string, dst:string) :unit = 
        use p = new Aspose.Slides.Presentation(src)        
        let options = Aspose.Slides.Export.PdfOptions(JpegQuality = byte 90, SaveMetafilesAsPng = true,
                        TextCompression = Aspose.Slides.Export.PdfTextCompression.Flate,
                        Compliance = Aspose.Slides.Export.PdfCompliance.Pdf15)        
        p.Save(dst, Aspose.Slides.Export.SaveFormat.Pdf) |> ignore

    let convert (src:string, dst:string, f) :unit = 
        let started = System.DateTime.Now
        f (src, dst)
        let finished = System.DateTime.Now
        printfn "conversion_time,%A,%A" (Path.GetFileName(src)) (finished - started).TotalMilliseconds

    let ignoreConverter (src:string, dst:string) :unit =
        printfn "ignoring %A" src
    
    let converterForExtension = function
        | ".doc" | ".docx" -> convertWord
        | ".ppt" | ".pptx" -> convertSlide
        | ".xls" | ".xlsx" -> convertSpreadsheet
        | _ -> ignoreConverter

    let run (args:Args) :unit =         
        Directory.CreateDirectory(args.outputFolder) |> ignore
        Directory.GetFiles(args.inputFolder) 
        |> Seq.ofArray
        |> Seq.map (fun srcPath -> 
                        (srcPath, 
                         Path.Combine(args.outputFolder, Path.GetFileNameWithoutExtension(srcPath) + ".pdf"),
                         converterForExtension(Path.GetExtension(srcPath))))
        |> Seq.iter convert 
        |> ignore

    [<EntryPoint>]
    let main argv =       
        match (parseArgs <| List.ofArray argv) with
            | Some args ->                 
                let started = System.DateTime.Now                
                run args
                printfn "conversion_time_total %A" (System.DateTime.Now - started)
                0
            | None -> 
                printfn "Usage: faspose <infolder> <outfolder>"
                1
