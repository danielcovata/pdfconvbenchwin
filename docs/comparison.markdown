# Office to PDF converter solutions

## Independent wholemeal solutions

[Aspose.Net](http://www.aspose.com/.net/total-component.aspx)

[Spire.Office for .NET](http://www.e-iceblue.com/Introduce/spire-office-for-net.html#.VDM9wUvsc8M)


## Interop & Printer driver solutions

[Office Interop](http://msdn.microsoft.com/en-us/library/bb157892.aspx)

[Sautinsoft UseOffice .Net](http://www.sautinsoft.com/convert-rtf-html-doc-docx-xls-xlsx-ppt-to-pdf-net-library/overview.php)

[Universal Document Converter](http://www.print-driver.com)


## Word only, wholemeal
[WordGlue .NET](http://www.websupergoo.com/products.htm) - only Word documents


## PDF libraries

Only for creating pdfs, would require something else for working with office docs.

[PDFCreator](http://www.pdfforge.org/pdfcreator/business)

[PDFsharp](http://www.pdfsharp.net/wiki/PDFsharpFAQ.ashx)


## TBD

[PrimoPDF](http://www.primopdf.com) - Only Word conversion


## Dismissed  

+ [Docmosis](http://www.docmosis.com) - web service
+ [LibreOffice](http://api.libreoffice.org/docs/install.html) - requires LibreOffice server running



## Comparisons

### Aspose.Net

Office -> PDF

Already use Aspose for Java
Reduced licensing costs
Wholemeal solution (Words, Cells, Slides, ...)
Similar API to Aspose Java
Good performance (80% speed of JVM though?)
Not cheap

### Spire.Office

Good licence (number of developers, unlimited deploys)
Standalone libraries
Wholemeal solution (word, xls, presentation, pdf)
Not cheap but discount available ($10800 OEM / developer)
[discounts](http://www.e-iceblue.com/Tutorials/Licensing/How-to-Get-Discount.html)

### PDFCreator

PDF Printer

TBD Price.  Free version is AGPL, need a business licence for 
multi-machine deployments.

Requires installation (eg, MSI rollout via AD).
Needs office-interop (or equivalent)


### Office Interop

Microsoft technology
Support may be problematic ('office wasn't designed for this'):
[Considerations for server-side](http://support2.microsoft.com/default.aspx?scid=kb;EN-US;q257757)
Optimal fidelity (assumed)
Requires Office to be installed
Performance is fair (4s / 3MB document, can parallelise by process)
(office 2007 before sp2 needs add-in pdf saver installed)

Can’t parallelise with threads
Requests are serialised to the COM object
Attempts will give RPC_E_SERVERCALL_RETRYLATER
Could use multiple word processes…scary
Interative prompts / modal UI popping up

Aspose Recommends against (business motivations?):
http://www.aspose.com/docs/display/wordsnet/Why+not+Automation

Stability - UI popups:

+ ‘install on first use’ - prompts user what to do
+ Configuring office - not modal, 'should' be ok
+ ‘there’s file you can recover, would you like to?’

Creates intermediate files (possible access conflicts, garbage collection needed)
  -shadow copies in source folder (have .docx, but invalid .docx files)
  -temp files in destination folder

Can use embedded types (.NET 4+, preferred) or PIA
TBD - can we target multiple office versions without issue using embeded types?

redistributables for PIA:

[2010](http://www.microsoft.com/en-us/download/details.aspx?id=3508)


### Sautinsoft UseOffice .Net

Requires Office to be installed
Cost effective (4 Dev licence, $1050 USD, can be deployed in application)


### PDFsharp

Free
No installation (standalone assemblies)

### Universal Document Converter

Requires office
Requires installation
Very expensive (100 users = $2000)

### PrimoPDF

Only Word
TBD Price
TBD Performance

## Hybrid Solutions

+ Use interop for machines with office installed, fallback to server conversion.
+ 

## Other Resources

[Considerations for server-side automation of office](http://support2.microsoft.com/kb/257757)
