Office 2007

+ Need the Save as Pdf addin installed otherwise 'feature not installed'
+ Continuous 'configuring for first use' dialog pops on when opening document
  * Need to add NoReReg key to prevent (version dependent):
    eg add HKCU\Software\Microsoft\Office\12.0\Word\Options /v NoReReg /t REG_DWORD /d 1
    See [here](http://social.technet.microsoft.com/Forums/en-US/7e14f498-485c-40c5-b522-d36d9b0c5697/office-2007-configuration-wizard-runs-every-time-i-start-any-application-word-excel-etc?forum=officesetupdeploylegacy)
+ stdole32l.tlb modal dialog box 
+ COMException on ExportAsFixedFormat: "the export failed due to an unexpected error"
+ COMException 'file not found' - removing/readding com objects fixes... wtf?

+ Downgrading Office installs is very problematic...

Password protected documents (ie, Excel spreadsheet)
 + TBD - Can we detect needing a password?
